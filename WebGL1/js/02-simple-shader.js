﻿"use strict";

window.addEventListener('DOMContentLoaded', function () {
	let shaderProgram;
	let vertexBuffer;
	let vertices;
	let gl;

	init();

	function init() {
		initWebGLContext();
		initShaderProgram();
		getUniforms();
		initGeometry();
		animate();
	}

	function initWebGLContext() {
		let canvas = document.getElementById("glcanvas");
		gl = canvas.getContext("webgl");
		gl.canvas.width = 500;
		gl.canvas.height = 500;
		gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);
	}

	function initShaderProgram() {
		let v = document.getElementById("vertex").firstChild.nodeValue;
		let f = document.getElementById("fragment").firstChild.nodeValue;

		let vs = gl.createShader(gl.VERTEX_SHADER);
		gl.shaderSource(vs, v);
		gl.compileShader(vs);

		let fs = gl.createShader(gl.FRAGMENT_SHADER);
		gl.shaderSource(fs, f);
		gl.compileShader(fs);

		shaderProgram = gl.createProgram();
		gl.attachShader(shaderProgram, vs);
		gl.attachShader(shaderProgram, fs);
		gl.linkProgram(shaderProgram);

		if (!gl.getShaderParameter(vs, gl.COMPILE_STATUS))
			console.log(gl.getShaderInfoLog(vs));

		if (!gl.getShaderParameter(fs, gl.COMPILE_STATUS))
			console.log(gl.getShaderInfoLog(fs));

		if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS))
			console.log(gl.getProgramInfoLog(shaderProgram));

		gl.useProgram(shaderProgram);

	}

	function getUniforms() {
		shaderProgram.uColor = gl.getUniformLocation(shaderProgram, "uColor");
		gl.uniform4fv(shaderProgram.uColor, [0.0, 1.0, 0.0, 1.0]);

		shaderProgram.aVertexPosition = gl.getAttribLocation(shaderProgram, "aVertexPosition");
		gl.enableVertexAttribArray(shaderProgram.aVertexPosition);
	}

	function initGeometry() {
		vertices = new Float32Array([-0.5, 0.5, 0.0,
			0.5, -0.5, 0.0,
		-0.5, -0.5, 0.0]);

		vertexBuffer = gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
		gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);

		vertexBuffer.itemSize = 3;
		vertexBuffer.numItems = vertices.length / vertexBuffer.itemSize;
	}

	function draw() {
		gl.vertexAttribPointer(shaderProgram.aVertexPosition, vertexBuffer.itemSize, gl.FLOAT, false, 0, 0);

		gl.clearColor(0, 0.5, 0, 1);
		gl.clear(gl.COLOR_BUFFER_BIT);

		gl.drawArrays(gl.TRIANGLES, 0, vertexBuffer.numItems);
	}

	function animate() {
		requestAnimationFrame(animate);
		draw();
	}

});
