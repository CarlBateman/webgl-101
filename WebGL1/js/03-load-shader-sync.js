﻿"use strict";

window.addEventListener('DOMContentLoaded', function () {
	let shaderProgram;
	let vertexBuffer;
	let vertices;
	let gl;
	let uniforms = []

	init();

	function init() {
		initWebGLContext();

		// some or all of the following should be done asynchronusly
		initShaderProgram();
		uniforms = getUniforms();
		getAttributes();
		initGeometry();
		animate();
	}

	function initWebGLContext() {
		let canvas = document.getElementById("glcanvas");
		gl = canvas.getContext("webgl");
		gl.canvas.width = 500;
		gl.canvas.height = 500;
		gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);
	}

	
	function readShaderFromFile(shaderURL) {
		let xmlhttp = new XMLHttpRequest();

		xmlhttp.onreadystatechange = function () {
			if (this.readyState == 4 && this.status == 200) {
				//console.log("onreadystatechange");
				return this.responseText;
			}
		};

		// NOTE: GET synchronously
		xmlhttp.open("GET", shaderURL, false);
		xmlhttp.send();

		//console.log("xmlhttp.responseText");
		return xmlhttp.responseText;
	}

	function initShaderProgram() {
		let v = readShaderFromFile("shaders/simple.vert");
		let f = readShaderFromFile("shaders/simple.frag");

		let vs = gl.createShader(gl.VERTEX_SHADER);
		gl.shaderSource(vs, v);
		gl.compileShader(vs);

		let fs = gl.createShader(gl.FRAGMENT_SHADER);
		gl.shaderSource(fs, f);
		gl.compileShader(fs);

		shaderProgram = gl.createProgram();
		gl.attachShader(shaderProgram, vs);
		gl.attachShader(shaderProgram, fs);
		gl.linkProgram(shaderProgram);

		if (!gl.getShaderParameter(vs, gl.COMPILE_STATUS))
			console.log(gl.getShaderInfoLog(vs));

		if (!gl.getShaderParameter(fs, gl.COMPILE_STATUS))
			console.log(gl.getShaderInfoLog(fs));

		if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS))
			console.log(gl.getProgramInfoLog(shaderProgram));

		gl.useProgram(shaderProgram);

	}

	function getAttributes() {
		//shaderProgram.uColor = gl.getUniformLocation(shaderProgram, "uColor");
		//gl.uniform4fv(shaderProgram.uColor, [0.0, 1.0, 0.0, 1.0]);

		shaderProgram.aVertexPosition = gl.getAttribLocation(shaderProgram, "aVertexPosition");
		gl.enableVertexAttribArray(shaderProgram.aVertexPosition);
	}

	function getUniforms() {
		let uniforms = [];
		let textures2D = [];
		let textures3D = [];

		let count = gl.getProgramParameter(shaderProgram, gl.ACTIVE_UNIFORMS);

		for (let i = 0; i < count; i++) {
			const info = gl.getActiveUniform(shaderProgram, i);

			switch (info.type) {

				case gl.SAMPLER_2D:
					textures2D[info.name] = {
						location: gl.getUniformLocation(shaderProgram, info.name),
						numOfTextures: numOfTextures
					};
					// assign numOfTextures???
					gl.Uniform1i(gl.getUniformLocation(shaderProgram, info.name), numOfTextures++);
					break;
				case gl.SAMPLER_3D:
					textures3D[info.name] = {
						location: gl.getUniformLocation(shaderProgram, info.name),
						numOfTextures: numOfTextures
					};
					// assign numOfTextures???
					gl.Uniform1i(gl.getUniformLocation(shaderProgram, info.name), numOfTextures++);
					break;
				default:
					uniforms[info.name] = { location: gl.getUniformLocation(shaderProgram, info.name), type: info.type };
			}
		}
		return uniforms;
	}


	function initGeometry() {
		vertices = new Float32Array([-0.5, 0.5, 0.0,
																	0.5, -0.5, 0.0,
																-0.5, -0.5, 0.0]);

		vertexBuffer = gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
		gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);

		vertexBuffer.itemSize = 3;
		vertexBuffer.numItems = vertices.length / vertexBuffer.itemSize;
	}

	function draw() {
		gl.uniform4fv(uniforms.uColor.location, [0.0, 1.0, 0.0, 1.0]);


		gl.vertexAttribPointer(shaderProgram.aVertexPosition, vertexBuffer.itemSize, gl.FLOAT, false, 0, 0);

		gl.clearColor(0, 0.5, 0, 1);
		gl.clear(gl.COLOR_BUFFER_BIT);

		gl.drawArrays(gl.TRIANGLES, 0, vertexBuffer.numItems);
	}

	function animate() {
		requestAnimationFrame(animate);
		draw();
	}

});
